package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/pkg/errors"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func part1(nums []int64) int64 {
	var result int64
	for _, n := range nums {
		result += n
	}
	return result
}

func part2(nums []int64) int64 {
	seen := make(map[int64]bool)

	var sum int64
Outer:
	for {
		for _, n := range nums {
			sum += n
			if _, ok := seen[sum]; ok {
				break Outer
			}
			seen[sum] = true
		}
	}
	return sum
}

func main() {
	var fname string
	flag.StringVar(&fname, "file", "input.txt", "Input file name")
	flag.Parse()

	f, err := os.Open(fname)
	check(errors.Wrap(err, "while opening file"))

	scanner := bufio.NewScanner(f)
	nums := make([]int64, 0, 100)
	for scanner.Scan() {
		s := scanner.Text()
		num, err := strconv.ParseInt(s, 10, 64)
		check(errors.Wrapf(err, "while parsing input as integer: %v", s))
		nums = append(nums, num)
	}
	check(errors.Wrap(scanner.Err(), "while reading from scanner"))

	frequency := part1(nums)
	fmt.Println("Output part-1: " + strconv.FormatInt(frequency, 10))

	repeatedFrequency := part2(nums)
	fmt.Println("Output part-2: " + strconv.FormatInt(repeatedFrequency, 10))
}
