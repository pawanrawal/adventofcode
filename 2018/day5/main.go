package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"unicode"

	"github.com/pkg/errors"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// Returns the length of result polymer after reacting is complete.
func reactPolymer(b []byte) int {
	text := []rune(strings.TrimSpace(string(b)))
	for i := 0; i < len(b)-1; i++ {
		buf := bytes.Buffer{}
		// Now compare pairs of consecutive characters and make them react with each other if
		// applicable.
		for idx := 0; idx < len(text); {
			first := text[idx]
			// This is last character, writeRune and break out.
			if idx+1 == len(text) {
				buf.WriteRune(first)
				break
			}

			second := text[idx+1]
			if unicode.ToLower(first) == unicode.ToLower(second) && first != second {
				// skip writing these two.
				idx += 2
			} else {
				buf.WriteRune(first)
				idx++
			}
		}
		tmp := buf.String()
		if len(tmp) == len(text) {
			// We couldn't make any two elements react, break
			text = []rune(tmp)
			break
		}
		text = []rune(tmp)
	}
	return len(text)
}

func main() {
	var fname string
	flag.StringVar(&fname, "file", "input.txt", "Input file name")
	flag.Parse()

	f, err := os.Open(fname)
	check(errors.Wrap(err, "while opening file"))

	b, err := ioutil.ReadAll(f)
	check(errors.Wrap(err, "while reading file"))

	fmt.Printf("Output: %v\n", reactPolymer(b))

	letters := "abcdefghijklmnopqrstuvwxyz"
	results := make(chan int, 26)
	for _, l := range letters {
		// Get the byte slice after removing this letter and pass it to reactPolymer.
		tempBuffer := make([]byte, len(b))
		copy(tempBuffer, b)
		tempBuffer = bytes.Replace(tempBuffer, []byte(string(l)), []byte(""), -1)
		tempBuffer = bytes.Replace(tempBuffer, []byte(string(unicode.ToUpper(l))), []byte(""), -1)
		go func(b []byte) {
			results <- reactPolymer(tempBuffer)
		}(tempBuffer)
	}

	minPolymerLength := len(b)
	for range letters {
		if polymerLength := <-results; polymerLength < minPolymerLength {
			minPolymerLength = polymerLength
		}
	}
	fmt.Printf("Output part-2: %v\n", minPolymerLength)
}
