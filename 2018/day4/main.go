package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/pkg/errors"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func assert(b bool, msg string, args ...interface{}) {
	if !b {
		log.Fatalf(msg, args)
	}
}

const (
	begin  = "begin"
	sleep  = "falls asleep"
	wakeup = "wakes up"
)

type event struct {
	ts      time.Time
	guardID uint
	typ     string // one of begin, sleep and wakeup
}

func scanInput(scanner *bufio.Scanner) []event {
	events := make([]event, 0, 10)
	for scanner.Scan() {
		var e event
		txt := scanner.Text()

		var year, month, day, hour, min int
		n, err := fmt.Sscanf(txt, "[%d-%d-%d %d:%d]", &year, &month, &day, &hour, &min)
		check(errors.Wrapf(err, "while scanning line: %v", txt))
		assert(n == 5, "expected to parse 5 input")
		e.ts = time.Date(year, time.Month(month), day, hour, min, 0, 0, time.UTC)

		// reset txt to remove the first part of the text
		i := strings.Index(txt, "]")
		if i == -1 || i+2 >= len(txt) {
			log.Fatalf("expected to find ] and other chars after it")
		}
		txt = txt[i+2:]

		n, _ = fmt.Sscanf(txt, "Guard #%d begins shift", &e.guardID)
		// Not checking error here because input doesn't need to conform to this format.
		if n != 0 {
			e.typ = begin
		} else {
			if !(txt == wakeup || txt == sleep) {
				log.Fatalf("expected one of wakesup/sleep, got: %v", txt)
			}
			e.typ = txt
		}
		events = append(events, e)
	}
	check(errors.Wrap(scanner.Err(), "while reading from scanner"))
	return events
}

func frequencyMap(events []event) map[uint][60]int {
	sleepFrequency := make(map[uint][60]int)
	var id uint
	var sleepTs time.Time
	for _, e := range events {
		//fmt.Println(e.ts.Format(time.RFC3339), e.typ, e.guardID)
		switch e.typ {
		case begin:
			id = e.guardID
		case sleep:
			sleepTs = e.ts
		case wakeup:
			startMin := sleepTs.Minute()
			endMin := e.ts.Minute()
			assert(id != 0, "guard id shouldn't be zero")
			freq := sleepFrequency[id]
			for i := startMin; i < endMin; i++ {
				freq[i]++
			}
			sleepFrequency[id] = freq
		}
	}
	return sleepFrequency
}

func mostAsleepGuard(sleepFrequency map[uint][60]int) (sleepyGuard uint, min int) {
	var maxSum int
	// First we find the sleepyGuard based on the total time they are asleep.
	for guardID, freqSlice := range sleepFrequency {
		localSum := 0
		for _, freq := range freqSlice {
			localSum += freq
		}
		if localSum > maxSum {
			sleepyGuard = guardID
			maxSum = localSum
		}
	}

	// Now we find the minute that they are most sleepy at.
	freqSlice := sleepFrequency[sleepyGuard]
	mostAsleepAt := 0
	for min, freq := range freqSlice {
		if freq > freqSlice[mostAsleepAt] {
			mostAsleepAt = min
		}
	}
	min = mostAsleepAt
	return
}

func mostSleepyGuardPart2(sleepFrequency map[uint][60]int) (sleepyGuard uint, min int) {
	maxFrequency := 0
	for guardID, freqSlice := range sleepFrequency {
		for m, freq := range freqSlice {
			if freq > maxFrequency {
				sleepyGuard = guardID
				min = m
				maxFrequency = freq
			}
		}
	}
	return
}

func main() {
	// First, parse input, sort it by timestamp.

	// Have a map of guardID => [60]int to keep a track of number of times a guard has been asleep
	// during a particular minute during the midnight hour.

	// Also optionally keep the guardID, minute and number of times they were asleep for the most
	// asleep guard in temporary variables.

	var fname string
	flag.StringVar(&fname, "file", "input.txt", "Input file name")
	flag.Parse()

	f, err := os.Open(fname)
	check(errors.Wrap(err, "while opening file"))

	scanner := bufio.NewScanner(f)
	events := scanInput(scanner)

	sort.Slice(events, func(i, j int) bool {
		return events[i].ts.Before(events[j].ts)
	})

	sleepFrequency := frequencyMap(events)
	sleepyGuard, mostAsleepAt := mostAsleepGuard(sleepFrequency)
	fmt.Printf("Output: %d\n", int(sleepyGuard)*mostAsleepAt)

	sleepyGuard, mostAsleepAt = mostSleepyGuardPart2(sleepFrequency)
	fmt.Printf("Output part-2: %d\n", int(sleepyGuard)*mostAsleepAt)
}
