package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/pkg/errors"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func inspectId(id string) (bool, bool) {
	freq := make(map[rune]uint8)
	for _, r := range id {
		freq[r]++
	}

	occursTwice, occursThrice := false, false
	for _, c := range freq {
		if c == 2 {
			occursTwice = true
		} else if c == 3 {
			occursThrice = true
		}
	}
	return occursTwice, occursThrice
}

func part1(ids []string) uint64 {
	twiceCount, thriceCount := 0, 0
	for _, id := range ids {
		twice, thrice := inspectId(id)
		if twice {
			twiceCount += 1
		}
		if thrice {
			thriceCount += 1
		}
	}

	return uint64(twiceCount * thriceCount)
}

func isSimilar(id1, id2 string) int {
	diffIndex := -1 // We store the first index that the ids are different at in this variable.
	if len(id1) != len(id2) {
		return diffIndex
	}

	numDifferences := 0
	for i := range id1 {
		if id1[i] != id2[i] {
			// If we have already seen a difference that means this is the second difference and
			// hence the boxes are not similar.
			if numDifferences == 1 {
				return -1
			}
			diffIndex = i
			numDifferences++
		}
	}
	return diffIndex
}

func findBoxes(ids []string) string {
	for i := range ids {
		for j := range ids {
			if i == j {
				continue
			}
			similarityIndex := isSimilar(ids[i], ids[j])
			if similarityIndex == -1 {
				continue
			}
			if similarityIndex == len(ids)-1 {
				return ids[i][:similarityIndex]
			} else {
				return ids[i][:similarityIndex] + ids[i][similarityIndex+1:]
			}
		}
	}
	return ""
}

func main() {
	var fname string
	flag.StringVar(&fname, "file", "input.txt", "Input file name")
	flag.Parse()

	f, err := os.Open(fname)
	check(errors.Wrap(err, "while opening file"))

	scanner := bufio.NewScanner(f)
	ids := make([]string, 0, 100)
	for scanner.Scan() {
		id := scanner.Text()
		ids = append(ids, id)
	}
	check(errors.Wrap(scanner.Err(), "while reading from scanner"))

	checksum := part1(ids)
	fmt.Printf("Output: %v\n", checksum)

	commonLetters := findBoxes(ids)
	fmt.Printf("Common: %s\n", commonLetters)
}
