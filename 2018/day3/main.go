package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/pkg/errors"
)

type claim struct {
	id     uint64
	left   uint64
	top    uint64
	width  uint64
	height uint64
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func parseClaim(line string) (claim, error) {
	c := claim{}
	num := ""
	for _, r := range line {
		switch r {
		case ' ', '#':
			continue
		case '@', ',', ':', 'x':
			n, err := strconv.ParseUint(num, 10, 64)
			if err != nil {
				return c, errors.Wrapf(err, "while parsing num as uint: %v", num)
			}

			switch r {
			case '@':
				c.id = n
			case ',':
				c.left = n
			case ':':
				c.top = n
			case 'x':
				c.width = n

			}
			num = ""
		default:
			num += string(r)
		}
	}

	n, err := strconv.ParseUint(num, 10, 64)
	if err != nil {
		return c, errors.Wrapf(err, "while parsing num as uint: %v", num)
	}
	c.height = n

	return c, nil
}

func overlaps(cl claim, freq map[string]uint16) bool {
	for i := cl.left; i < cl.left+cl.width; i++ {
		for j := cl.top; j < cl.top+cl.height; j++ {
			key := strconv.Itoa(int(i)) + "-" + strconv.Itoa(int(j))
			if freq[key] != 1 {
				return true
			}
		}
	}
	return false
}

func main() {
	var fname string
	flag.StringVar(&fname, "file", "input.txt", "Input file name")
	flag.Parse()

	f, err := os.Open(fname)
	check(errors.Wrap(err, "while opening file"))

	freq := make(map[string]uint16)
	var claims []claim
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		cl, err := parseClaim(line)
		check(errors.Wrapf(err, "while parsing line: %v", line))

		for i := cl.left; i < cl.left+cl.width; i++ {
			for j := cl.top; j < cl.top+cl.height; j++ {
				key := strconv.Itoa(int(i)) + "-" + strconv.Itoa(int(j))
				freq[key]++
			}
		}
		claims = append(claims, cl)
	}

	// part1
	squareMetres := 0
	for _, v := range freq {
		if v >= 2 {
			squareMetres++
		}
	}
	fmt.Printf("Output: %v\n", squareMetres)

	// part2
	for _, cl := range claims {
		if overlaps(cl, freq) {
			continue
		}
		fmt.Printf("Output, claimId: %v\n", cl.id)
		break
	}
}
